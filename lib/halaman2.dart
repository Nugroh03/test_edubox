import 'package:flutter/material.dart';
import 'package:flutter_application_1/halaman1.dart';

class HalamanKEdua extends StatefulWidget {
  const HalamanKEdua({Key? key, this.value}) : super(key: key);

  final double? value;

  @override
  State<HalamanKEdua> createState() => _HalamanKEduaState();
}

class _HalamanKEduaState extends State<HalamanKEdua> {
  double newvalue = 0;
  TextEditingController nilaiNewController = TextEditingController();
  String? input;
  bool input2 = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  child: ElevatedButton(
                      onPressed: () {
                        setState(() {
                          input2 = true;
                          input = "+";
                        });
                      },
                      child: Text("+")),
                ),
                Container(
                  child: ElevatedButton(
                      onPressed: () {
                        setState(() {
                          input2 = true;
                          input = "-";
                        });
                      },
                      child: Text("-")),
                ),
                Container(
                  child: ElevatedButton(
                      onPressed: () {
                        setState(() {
                          input2 = true;
                          input = "/";
                        });
                      },
                      child: Text("/")),
                ),
              ],
            ),
            if (input2)
              Container(
                child: TextFormField(
                  controller: nilaiNewController,
                  decoration: InputDecoration(hintText: "Masukkan Angka"),
                ),
              ),
            ElevatedButton(
                onPressed: () {
                  if (input == "+") {
                    setState(() {
                      newvalue =
                          widget.value! + double.parse(nilaiNewController.text);
                      newvalue = newvalue;
                    });
                  } else if (input == "-") {
                    setState(() {
                      newvalue =
                          widget.value! - double.parse(nilaiNewController.text);
                      newvalue = newvalue;
                    });
                  } else {
                    setState(() {
                      newvalue =
                          widget.value! / double.parse(nilaiNewController.text);
                      newvalue = newvalue;
                    });
                  }
                  print("new" + newvalue.toString());
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext) => HalamanPertama(
                                value: newvalue,
                              )));
                },
                child: Container(
                  child: Text("Lanjutkan"),
                ))
          ],
        ),
      ),
    );
  }
}
