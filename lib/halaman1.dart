import 'package:flutter/material.dart';
import 'package:flutter_application_1/halaman2.dart';

class HalamanPertama extends StatelessWidget {
  const HalamanPertama({Key? key, this.value}) : super(key: key);

  final double? value;

  @override
  Widget build(BuildContext context) {
    TextEditingController nilaiController = TextEditingController();
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextFormField(
                controller: nilaiController,
                decoration: InputDecoration(hintText: "Masukkan Angka")),
            ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext) => HalamanKEdua(
                                value: double.parse(nilaiController.text),
                              )));
                },
                child: Container(
                  child: Text("Lanjutkan"),
                )),
            Text(value.toString())
          ],
        ),
      ),
    );
  }
}
